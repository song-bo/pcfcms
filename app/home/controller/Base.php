<?php
/***********************************************************
 * 基础控制器
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\home\controller;
use app\common\controller\Common;
use app\home\logic\FieldLogic;
class Base extends Common
{
    public function initialize()
    {
        parent::initialize();
        $this->fieldLogic = new FieldLogic();
        $this->pcfglobal = get_global();
        // 设置URL模式
        set_home_url_mode();
    }

}
