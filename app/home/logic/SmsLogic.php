<?php
/***********************************************************
 * 短信逻辑定义
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\home\logic;
use think\facade\Db;
use think\facade\Request;
use think\facade\Cache;
use think\facade\Session;

class SmsLogic
{
    // 发送短信
    public function send_sms($phone = '',$type = 'reg')
    {
        if (empty($phone)) {
            return ['code'=>0, 'msg'=>"手机号码参数不能为空！"];
        }
        // 查询扩展是否开启
        $openssl_funcs = get_extension_funcs('openssl');
        if (!$openssl_funcs) {
            return ['code'=>0, 'msg'=>"请联系空间商，开启php的 <font color='red'>openssl</font> 扩展！"];
        }
        // 是否填写短信配置
        $sms_config = tpCache('sms');
        if($sms_config['sms_syn_weapp'] == 1 || !empty($sms_config['sms_syn_weapp'])){
            unset($sms_config['sms_content']);
            unset($sms_config['sms_test']);
            foreach ($sms_config as $key => $val) {
                if (empty($val)) {
                    return array('code'=>0 , 'msg'=>'该功能待开放，网站管理员尚未完善短信配置！');
                }
            }
        }else{
            return array('code'=>0 , 'msg'=>'后台短信功能没有开启！');
        }
        $users_id = session::get('pcfcms_users_id');
        if ('retrieve_password' == $type) //找回密码
        {
            // 判断会员是否已绑定短信
            $usersdata = Db::name('users')->where('mobile',$phone)->field('mobile,status')->find();
            if (!empty($usersdata)) {
                if (empty($usersdata['status']) || $usersdata['status'] == 0) {
                    return ['code'=>0, 'msg'=>'该会员尚未激活，不能找回密码！'];
                } else if (empty($usersdata['mobile'])) {
                    return ['code'=>0, 'msg'=>'手机号码未绑定，不能找回密码！'];
                }
                // 数据添加
                $datas['source']    = 4; // 来源，与场景ID对应：0=默认，2=注册，3=绑定手机，4=找回密码
                $datas['mobile']     = $phone;
                $datas['code']      = rand(1000,9999);
                $datas['add_time']  = time();
                Db::name('sms_log')->save($datas);
            }else{
                return ['code'=>0, 'msg'=>'手机号码不存在！'];
            }
        }
        else if ('bind_sms' == $type) //绑定手机
        {
            // 判断手机是否已存在
		    $listwhere=[];
			$listwhere[]=['mobile','=',$phone];
			$listwhere[]=['id','<>',$users_id];
            $users_list = Db::name('users')->where($listwhere)->find();
            if(empty($users_list)){
                // 判断会员是否已绑定相同手机
				$userswhere=[];
				$userswhere=['id','=',$users_id];
				$userswhere=['mobile','=',$phone];
                $usersdata = Db::name('users')->where($userswhere)->field('mobile')->find();
                if (!empty($usersdata['mobile'])) {
                    return ['code'=>0, 'msg'=>'手机已绑定，无需重新绑定！'];
                }
                $datas['source']    = 3; // 来源，与场景ID对应：0=默认，2=注册，3=绑定手机，4=找回密码
                $datas['mobile']     = $phone;
                $datas['users_id']  = $users_id;
                $datas['code']      = rand(1000,9999);
                $datas['add_time']  = time();
                Db::name('sms_log')->save($datas);
            }else{
                return ['code'=>0, 'msg'=>"手机已经存在，不可以绑定！"];
            }
        }
		else if ('reg' == $type) //注册
        {
            $users_list = Db::name('users')->where('mobile',$phone)->find();
            if (empty($users_list)) {
                $datas['source']    = 2; // 来源，与场景ID对应：0=默认，2=注册，3=绑定手机，4=找回密码
                $datas['mobile']     = $phone;
                $datas['code']      = rand(1000,9999);
                $datas['add_time']  = time();
                Db::name('sms_log')->save($datas);
            }else{
                return ['code'=>0, 'msg'=>'手机'.$phone.'已被注册'];
            }
        }

        if('admin' == $type) //提醒管理员
		{
			// 实例化类库，调用发送短信
			pcfcmssend_sms($phone);		
		}else{
			// 实例化类库，调用发送短信
			$res = pcfcmssend_sms($phone,$datas['code']);
			if (intval($res['code']) == 1) {
				return ['code'=>1, 'msg'=>$res['msg']];
			} else {
				return ['code'=>0, 'msg'=>$res['msg']];
			}			
		}

    }
}
