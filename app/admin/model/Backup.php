<?php
/***********************************************************
 * 数据库备份模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Session;
use think\facade\Env;

//数据导出模型
class Backup{

    //文件指针
    private $fp;
    //备份文件信息 part - 卷号，name - 文件名
    private $file;
    //当前打开文件大小
    private $size = 0;
    //数据库配置
    private $dbconfig = array();
    //备份配置
    private $config;

    /**
     * 数据库备份构造方法
     * @param array  $file   备份或还原的文件信息
     * @param array  $config 备份配置信息
     * @param string $type   执行类型，export - 备份数据， import - 还原数据
     */
    public function __construct($file, $config, $type = 'export')
    {
        $this->file   = $file;
        $this->config = $config;
        //初始化数据库连接参数
        $this->setDbConn();
    }

    /**
     * 打开一个卷，用于写入数据
     * @param  integer $size 写入数据的大小
     */
    private function open($size)
    {
        if($this->fp){
            $this->size += $size;
            if($this->size > $this->config['part']){
                $this->config['compress'] ? @gzclose($this->fp) : @fclose($this->fp);
                $this->fp = null;
                $this->file['part']++;
                session::set('backup_file', $this->file);
                $this->Backup_Init();
            }
        } else {
            $backuppath = $this->config['path'];
            $filename   = "{$backuppath}{$this->file['name']}-{$this->file['part']}-{$this->file['version']}.sql";
            if($this->config['compress']){
                $filename = "{$filename}.gz";
                $this->fp = @gzopen($filename, "a{$this->config['level']}");
            } else {
                $this->fp = @fopen($filename, 'a');
            }
            $this->size = filesize($filename) + $size;
        }
    }

    /**
     * 写入初始数据
     * @return boolean true - 写入成功，false - 写入失败
     */
    public function Backup_Init()
    {
        $sql = "-- -----------------------------\n";
        $sql .= "-- PcfWeb MySQL Data Transfer \n";
        $sql .= "-- Host     : " . $this->dbconfig['hostname'] . "\n";
        $sql .= "-- Port     : " . $this->dbconfig['hostport'] . "\n";
        $sql .= "-- Database : " . $this->dbconfig['database'] . "\n";
        $sql .= "-- Part : #{$this->file['part']}\n";
        $sql .= "-- Date : " . date("Y-m-d H:i:s") . "\n";
        $sql .= "-- -----------------------------\n\n";
        $sql .= "SET FOREIGN_KEY_CHECKS = 0;\n\n";
        return $this->write($sql);
    }

    /**
     * 写入SQL语句
     * @param  string $sql 要写入的SQL语句
     * @return boolean true - 写入成功，false - 写入失败！
     */
    private function write($sql)
    {
        $size = strlen($sql);
        //由于压缩原因，无法计算出压缩后的长度，这里假设压缩率为50%，
        //一般情况压缩率都会高于50%；
        $size = $this->config['compress'] ? $size / 2 : $size;
        $this->open($size);
        return $this->config['compress'] ? @gzwrite($this->fp, $sql) : @fwrite($this->fp, $sql);
    }

    /**
     * 备份表结构
     * @param  string  $table 表名
     * @param  integer $start 起始行数
     * @return boolean false - 备份失败
     */
    public function backup($table, $start)
    {
        if(0 == $start){
            $result = Db::query("SHOW CREATE TABLE `{$table}`");
            $sql  = "\n";
            $sql .= "-- -----------------------------\n";
            $sql .= "-- Table structure for `{$table}`\n";
            $sql .= "-- -----------------------------\n";
            $sql .= "DROP TABLE IF EXISTS `{$table}`;\n";
            $sql .= trim($result[0]['Create Table']) . ";\n\n";
            if(false === $this->write($sql)){
                return false;
            }
        }
        //数据总数
        $result = Db::query("SELECT COUNT(*) AS count FROM `{$table}`");
        $count  = $result['0']['count'];
        //备份表数据
        if($count){
            //写入数据注释
            if(0 == $start){
                $sql  = "-- -----------------------------\n";
                $sql .= "-- Records of `{$table}`\n";
                $sql .= "-- -----------------------------\n";
                $this->write($sql);
            }
            //备份数据记录
            $result = Db::query("SELECT * FROM `{$table}` LIMIT {$start}, 1000");
            foreach ($result as $row) {
                $row = array_map('addslashes', $row);
                $sql = "INSERT INTO `{$table}` VALUES ('" . str_replace(array("\r","\n"),array('\r','\n'),implode("', '", $row)) . "');\n";
                if(false === $this->write($sql)){
                    return false;
                }
            }
            //还有更多数据
            if($count > $start + 1000){
                return array($start + 1000, $count);
            }
        }
        //备份下一表
        return 0;
    }

    // 还原数据
    public function import($start)
    {
        if($this->config['compress']){
            $gz   = gzopen($this->file[1], 'r');
            $size = 0;
        } else {
            $size = filesize($this->file[1]);
            $gz   = fopen($this->file[1], 'r');
        }
        $sql  = '';
        if($start){
            $this->config['compress'] ? gzseek($gz, $start) : fseek($gz, $start);
        }
        $totalsize = 0;
        $sql  = '';
        for($i = 0; $i < 1000; $i++){
            $sql .= $this->config['compress'] ? gzgets($gz) : fgets($gz);
            if(preg_match('/.*;$/', trim($sql))){
                //执行SQL语句
                $sqlFormat = $this->sql_split($sql, config('database.connections.mysql.prefix'));
                $counts = count($sqlFormat);
                for ($j = 0; $j < $counts; $j++) {
                    $tmp_sql = trim($sqlFormat[$j]);
                    if (strstr($tmp_sql, 'CREATE TABLE')) {
                        if(false !== Db::execute($tmp_sql)){
                            $start += strlen($tmp_sql);
                        } else {
                            return false;
                        }
                    } else {
                        if(trim($tmp_sql) == '')
                           continue;
                        if(false !== Db::execute($tmp_sql)){
                            $start += strlen($tmp_sql);
                        } else {
                            return false;
                        }
                    }
                }
                $sql = '';
            } elseif ($this->config['compress'] ? gzeof($gz) : feof($gz)) {
                return 0;
            }
        }
        return array($start, $size, $totalsize);
    }

    // 解析
    public static function parseSql($filename)
    {
        /*获取所有数据表名*/
        $tableList = [];
        $dbtables = Db::query('SHOW TABLE STATUS');
        foreach ($dbtables as $k => $v) {
            if (preg_match('/^'.config('database.connections.mysql.prefix').'/i', $v['Name'])) {
                $tableList[] = $v['Name'];
            }
        }
        $lines=file($filename);
        $lines[0]=str_replace(chr(239).chr(187).chr(191),"",$lines[0]);//去除BOM头
        $flage = true;
        $sqls = array();
        $sql="";
        foreach($lines as $line)
        {
            $line=trim($line);
            $char=substr($line,0,1);
            if($char!='#' && strlen($line)>0)
            {
                $prefix=substr($line,0,2);
                switch($prefix)
                {
                    case '/*':
                    {
                        $flage=(substr($line,-3)=='*/;'||substr($line,-2)=='*/')?true:false;
                        break 1;
                    }
                    case '--': break 1;
                    default :
                    {
                        if($flage)
                        {
                            $sql.=$line;
                            if(substr($line,-1)==";")
                            {
                                /*处理安装sql与备份sql里表前缀的兼容性*/
                                $pcf_prefix = 'pcf_';
                                foreach ($tableList as $k2 => $v2) {
                                    if (stristr($sql, '`'.$v2.'`')) {
                                        $pcf_prefix = config('database.connections.mysql.prefix');
                                        break;
                                    }
                                }
                                if ('pcf_' != config('database.connections.mysql.prefix')) {
                                    $sql = str_replace("`{$pcf_prefix}", '`'.config('database.connections.mysql.prefix'), $sql);
                                }
                                $sql = preg_replace("/TYPE=(InnoDB|MyISAM|MEMORY)( DEFAULT CHARSET=[^; ]+)?/", "ENGINE=\\1 DEFAULT CHARSET=utf8", $sql);
                                $sqls[]=$sql;
                                $sql="";
                            }
                        }
                        if(!$flage)$flage=(substr($line,-3)=='*/;'||substr($line,-2)=='*/')?true:false;
                    }
                }
            }
        }
        return $sqls;
    }

    //安装
    public static function install($sqls)
    {
        $flag = true;
        if(is_array($sqls)){
            foreach($sqls as $sql){
                if(Db::execute($sql) === false){ $flag = false;}
            }
        }
        return $flag;
    }

    //数据库字符替换
    public function sql_split($sql, $tablepre) 
    {
        if ($tablepre != "pcf_")
            $sql = str_replace("`pcf_", '`'.$tablepre, $sql);
        $sql = preg_replace("/TYPE=(InnoDB|MyISAM|MEMORY)( DEFAULT CHARSET=[^; ]+)?/", "ENGINE=\\1 DEFAULT CHARSET=utf8", $sql);
        $sql = str_replace("\r", "\n", $sql);
        $ret = array();
        $num = 0;
        $queriesarray = explode(";\n", trim($sql));
        unset($sql);
        foreach ($queriesarray as $query) {
            $ret[$num] = '';
            $queries = explode("\n", trim($query));
            $queries = array_filter($queries);
            foreach ($queries as $query) {
                $str1 = substr($query, 0, 1);
                if ($str1 != '#' && $str1 != '-')
                    $ret[$num] .= $query;
            }
            $num++;
        }
        return $ret;
    }

    //设置数据库连接必备参数
    public function setDbConn($dbconfig = [])
    {
        if (empty($dbconfig)) {
            $this->dbconfig = config('database.connections.' . config('database.default'));
        } else {
            $this->dbconfig = $dbconfig;
        }
        return $this;
    }

    //析构方法，用于关闭文件资源
    public function __destruct()
    {
        $this->config['compress'] ? @gzclose($this->fp) : @fclose($this->fp);
    }


}
