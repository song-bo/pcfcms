<?php
/***********************************************************
 * 定时任务
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
use think\facade\Cache;
class Task extends Base
{
    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = Request::controller().'/index';
        $this->popedom = appfile_popedom($ctl_act);
    }
    public function index()
    {
        //验证权限
        if(!$this->popedom["list"]){
            return $this->errorNotice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            $post = input('param.');
            if(isset($post['limit'])){
                $limit = $post['limit'];
            }else{
                $limit = 10;
            }
            $list = Db::name('task')->paginate($limit);
            $newdata = $list->items();
            foreach ($newdata as $key => $value) {
                if($value['maxnum'] == 0){
                   $newdata[$key]['maxnum'] = '无限制'; 
                }
                $newdata[$key]['start_time'] = pcftime($value['start_time']);
                $newdata[$key]['stop_time'] = pcftime($value['stop_time']);
                $newdata[$key]['add_time'] = pcftime($value['add_time']);
                if(isset($value['update_time'])){
                   $newdata[$key]['update_time'] = pcftime($value['update_time']);
                }
            }
            $result = ['code' => 0, 'data' => $newdata,'count'=> $list->total()];
            return $result;
        }
        return $this->fetch();
    }

    public function add(){
        if (Request::isPost()) {
            //验证权限
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => 1, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => 1, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            }
            $post = input('param.');
            $add_data = array();
            $add_data['title'] = $post['title'];
            $add_data['maxnum'] = isset($post['maxnum']) ? $post['maxnum'] : 0;
            $add_data['start_time'] = strtotime($post['start_time']);
            $add_data['stop_time'] = strtotime($post['stop_time']);
            $add_data['content'] = $post['content'];
            $add_data['add_time'] = time(); //时间             
            if (Db::name('task')->save($add_data)) {
                $result = ['code' => 0, 'msg' => '添加成功','url'=>Request::baseFile().'/Task/index'];
                return $result;
            } else {
                $result = ['code' => 1, 'msg' => '添加失败'];
                return $result;
            }
        }
        return $this->fetch('add');
    }

    public function edit(){
        if (Request::isPost()) {
            //验证权限
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => 1, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => 1, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            } 
            $post = input('param.');
            $add_data = array();
            $add_data['id'] = $post['id'];
            $add_data['title'] = $post['title'];
            $add_data['maxnum'] = isset($post['maxnum']) ? $post['maxnum'] : 0;
            $add_data['start_time'] = strtotime($post['start_time']);
            $add_data['stop_time'] = strtotime($post['stop_time']);   
            $add_data['content'] = isset($post['content']) ? $post['content']:'';
            Db::name('task')->where('id', $add_data['id'])->data($add_data)->update();
            $result = ['code' => 0, 'msg' => '修改成功','url'=>Request::baseFile().'/Task/index'];
            return $result;
        }
        $info = Db::name('task')->where('id', input('param.id/d'))->find();
        $info['start_time'] = pcftime($info['start_time']);
        $info['stop_time'] = pcftime($info['stop_time']);
        $this->assign('info', $info);
        return $this->fetch('edit');
    }


    // 删除单个
    public function del(){
         if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $id = input('param.id/d');
            if (Db::name('task')->where("id",$id)->delete()) {
                $result = ['status' => true, 'msg' => '删除成功'];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '删除失败'];
                return $result;
            }
            return $result;
        }       
    }

    public function batch_del(){
         if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => 0, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => 0, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $id_arr = input('del_id/a');
            $id_arr = eyIntval($id_arr);
            if(is_array($id_arr) && !empty($id_arr)){
                foreach ($id_arr as $key => $val) {
                   Db::name('task')->where('id',$val)->delete();
                }
                $result = ['code' => 1, 'msg' => '删除成功！'];
                return $result;
            } else {
                $result = ['code' => 0, 'msg' => '参数有误'];
                return $result;
            }
        }       
    }

}