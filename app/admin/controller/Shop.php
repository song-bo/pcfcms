<?php
/***********************************************************
 * 官方模板资源
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
use think\facade\Cache;
class Shop extends Base
{
    public function initialize() {
        parent::initialize();
    }

    public function mf()
    {
        return $this->fetch();
    }

    public function sf()
    {
        return $this->fetch();
    }

}