<?php
/***********************************************************
 * 图片处理
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller\system;
use app\admin\controller\Base;
use think\facade\Request;
use think\facade\Cache;
use think\facade\Db;
class Water extends Base
{
    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = Request::controller().'/index';
        $this->popedom = appfile_popedom($ctl_act);
    }

    //图片水印
    public function index()
    {
        //验证查看权限
        if(!$this->popedom["list"]){
            return $this->errorNotice(config('params.auth_msg.list'),true,3,false);
        }
        $inc_type =  'water';
        if (Request::isAjax()) {
            //验证修改权限
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            $param = input('post.');
            tpCache($inc_type,$param);
            $result = ['status' => true, 'msg' => '操作成功'];
            return $result;  
        }
        $config = tpCache($inc_type);
        $this->assign('config',$config);//当前配置项
        return $this->fetch('index');
    }

    //缩略图配置
    public function thumb()
    {
        $inc_type =  'thumb';
        if (Request::isAjax()) {
            //验证修改权限
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            $param = input('post.');
            isset($param['thumb_width']) && $param['thumb_width'] = preg_replace('/[^0-9]/', '', $param['thumb_width']);
            isset($param['thumb_height']) && $param['thumb_height'] = preg_replace('/[^0-9]/', '', $param['thumb_height']);
            $thumbConfig = tpCache('thumb');
            tpCache($inc_type,$param);
            //校验配置是否改动，若改动将会清空缩略图目录
            if (md5(serialize($param)) != md5(serialize($thumbConfig))) {
                // 清除缓存
                Cache::clear();//清除数据缓存文件
                $admin_temp = glob(root_path() . 'runtime/admin/temp/'. '*.php');//清除后台临时文件缓存
                array_map('unlink', $admin_temp);
            }
            $result = ['status' => true, 'msg' => '操作成功'];
            return $result; 
        }
        $config = tpCache($inc_type);
        // 设置缩略图默认配置
        if (!isset($config['thumb_open'])) {
            $thumbextra = config('global.thumb');
            $param = [
                'thumb_open'    => $thumbextra['open'],
                'thumb_mode'    => $thumbextra['mode'],
                'thumb_color'   => $thumbextra['color'],
                'thumb_width'   => $thumbextra['width'],
                'thumb_height'  => $thumbextra['height'],
            ];
            tpCache($inc_type,$param);
            $config = tpCache($inc_type);
        }
        $this->assign('config',$config);
        return $this->fetch();
    }

    //标签调用的弹窗说明
    public function ajax_tag_call()
    {
        if (Request::isAjax()) {
            $name = input('post.name/s');
            $msg = '';
            switch ($name) {
                case 'thumb_open':
                    {
                        $msg = '
<div yne-bulb-block="paragraph">
    <span style="color:red">（温馨提示：高级调用不会受缩略图功能的开关影响！）</span></div>
<div yne-bulb-block="paragraph">
    【标签方法的格式】</div>
<div yne-bulb-block="paragraph">
    &nbsp;&nbsp;&nbsp;&nbsp;thumb_img=###,宽度,高度,生成方式</div>
<br data-filtered="filtered">
<div yne-bulb-block="paragraph">
    【指定宽高度的调用】</div>
<div yne-bulb-block="paragraph">
    &nbsp;&nbsp;&nbsp;&nbsp;列表页/内容页：{$gzpcf.field.litpic<span style="color:red">|thumb_img=###,500,500</span>}</div>
<div yne-bulb-block="paragraph">
    &nbsp;&nbsp;&nbsp;&nbsp;标签arclist/list里：{$field.litpic<span style="color:red">|thumb_img=###,500,500</span>}</div>
<br data-filtered="filtered">
<div yne-bulb-block="paragraph">
    【指定生成方式的调用】</div>
<div yne-bulb-block="paragraph">
    &nbsp;&nbsp;&nbsp;&nbsp;生成方式：1 = 拉伸；2 = 留白；3 = 截减；<br data-filtered="filtered">
    &nbsp;&nbsp;&nbsp;&nbsp;以标签arclist为例：</div>
<div yne-bulb-block="paragraph">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缩略图拉伸：{$field.litpic<span style="color:red">|thumb_img=###,500,500,1</span>}</div>
<div yne-bulb-block="paragraph">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缩略图留白：{$field.litpic<span style="color:red">|thumb_img=###,500,500,2</span>}</div>
<div yne-bulb-block="paragraph">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;缩略图截减：{$field.litpic<span style="color:red">|thumb_img=###,500,500,3</span>}</div>
<div yne-bulb-block="paragraph">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;默&nbsp;认&nbsp;生&nbsp;成：{$field.litpic<span style="color:red">|thumb_img=###,500,500</span>}&nbsp;&nbsp;&nbsp;&nbsp;(以默认全局配置的生成方式)</div>
';
                    }
                    break;
                default:
                    break;
            }
            $result = ['status' => true, 'msg' => '请求成功','data' =>$msg];
            return $result; 
        }
        $result = ['status' => false, 'msg' => '非法访问！'];
        return $result;
    }

}
