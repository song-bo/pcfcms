<?php
/***********************************************************
 * 所有ajax请求或者不经过权限验证的方法全放在这里
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
use think\facade\Cache;
use app\admin\logic\AjaxLogic;
use app\admin\logic\UpdateLogic;
class Ajax extends Base 
{
    private $ajaxLogic;
    public function initialize() {
        parent::initialize();
        $this->ajaxLogic = new AjaxLogic;
    }

    //进入欢迎页面需要异步处理的业务
    public function welcome_handle()
    {
        $this->ajaxLogic->welcome_handle();
    }

}