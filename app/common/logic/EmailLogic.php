<?php
/***********************************************************
 * 邮箱逻辑定义
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\common\logic;
use PHPMailer\PHPMailer\PHPMailer;

/*邮件类*/
class EmailLogic 
{

    /**
     * 邮件发送
     * @param $to    接收人
     * @param string $subject   邮件标题
     * @param string|array $content   邮件内容(html模板渲染后的内容)
     * @throws Exception
     */
    public function send_email($to='', $subject='', $content='',$smtp_config , $library = 'phpmailer'){
        switch ($library) {
            case 'phpmailer':
                return $this->send_phpmailer($to, $subject, $content,$smtp_config);
                break;
            default:
                return $this->send_phpmailer($to, $subject, $content,$smtp_config);
                break;
        }
    }

    /**
     * 邮件发送 - 第三方库phpmailer
     * @param $to    接收人
     * @param string $subject   邮件标题
     * @param string|array $content   邮件内容(html模板渲染后的内容)
     * @throws Exception
     */
    private function send_phpmailer($to='', $subject='', $content='',$smtp_config){
		$config = !empty($smtp_config) ? $smtp_config : tpCache('smtp');
        try {
            //判断openssl是否开启
            $openssl_funcs = get_extension_funcs('openssl');
            if(!$openssl_funcs){
                return array('code'=>0 , 'msg'=>'请先开启php的openssl扩展');
            }
			$mail = new PHPMailer;
            $mail->CharSet  = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            //接收者邮件
            empty($to) && $to = $config['smtp_test'];
            $to = explode(',', $to);
            //smtp服务器
            $mail->Host = $config['smtp_server'];
            //端口 - likely to be 25, 465 or 587
            $mail->Port = intval($config['smtp_port']);
            // 使用安全协议
            switch ($mail->Port) {
                case 465:
                    $mail->SMTPSecure = 'ssl';
                    break;
                case 587:
                    $mail->SMTPSecure = 'tls';
                    break;
                default:
                    # code...
                    break;
            }
            //Whether to use SMTP authentication
            $mail->SMTPAuth = true;
            //用户名
            $mail->Username = $config['smtp_user'];
            //密码
            $mail->Password = $config['smtp_pwd'];
            //Set who the message is to be sent from
            $mail->setFrom($config['smtp_user']);
            //接收邮件方
            if(is_array($to)){
                foreach ($to as $v){
                    $mail->addAddress($v);
                }
            }else{
                $mail->addAddress($to);
            }
            $mail->isHTML(true);
            //标题
            $mail->Subject = $subject;
            $mail->msgHTML($content);
            $result = $mail->send();
            if (!$result) {
                return array('code'=>0 , 'msg'=>'发送失败:'.$mail->ErrorInfo);
            } else {
                return array('code'=>1 , 'msg'=>'发送成功');
            }
        } catch (\Exception $e) {
            return array('code'=>0 , 'msg'=>'发送失败');
        }
    }


}
