<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;
use think\facade\Request;
use think\facade\Db;
class TagForm extends Base
{
    protected function initialize()
    {
        parent::initialize();
    }

    // 获取表单数据
    public function getForm($formid = '')
    {
        if (empty($formid)) {
            echo '标签form报错：缺少属性 formid 值。';
            return false;
        } else {
            $formid = intval($formid);
        }
        $form = Db::name('guestbook')->where('id' , $formid)->find();
        if (empty($form)){
            echo '标签form报错：该表单不存在。';
            return false;
        }
        $form_attr = Db::name('guestbook_attr')->where(['form_id' => $form['id'],'is_del' => 0])->order("sort_order asc, attr_id asc")->select()->toArray();
        if (empty($form_attr)){
            echo '标签form报错：表单没有新增字段。';
            return false;
        }
        $md5 = md5(time().uniqid(mt_rand(), true));
        $funname = 'pcfcms'.md5("gzpcf_form_token_{$form['id']}".$md5);
        $form_name = 'form_'.$funname;
        $token_id = md5('form_token_'.$form['id'].$md5);
        $submit = 'pcfcms'.$token_id;
        $result = array(); 
        //检测规则
        foreach ($form_attr as $key=>$val){
            $attr_id = $val['attr_id'];
            // 字段名称
            $name = 'attr_'.$attr_id;
            $result[$name] = $name;
            // 表单提示文字
            $itemname = 'itemname_'.$attr_id;
            $result[$itemname] = $val['attr_name'];
            // 筛选内容
            if (!empty($val['attr_values'])) {
                $tmp_option_val = explode(',', $val['attr_values']);
                $options = array();
                foreach($tmp_option_val as $k2=>$v2)
                {
                    $tmp_val = array('value' => $v2,);
                    array_push($options, $tmp_val);
                }
                $result['options_'.$attr_id] = $options;
            }
        }
        $action = request()->domain()."/api.php".url('/Ajax/form_submit', [], true, false, 1);
        $tokenStr = <<<EOF
<script type="text/javascript">
    function tijiao(){
        if(diypcfcms() == 0){
            return false;
        }else{
            send();
        }
    } 
    function send(){
        $.ajax({
            type: "POST",
            url: "{$action}",
            data: $("#{$form_name}").serialize(),
            dataType: "json",
            success: function(data){
                  if(data.status){
                      alert(data.msg);
                      window.location.reload();//刷新本页面
                  }else{
                      alert(data.msg);
                      window.location.reload();//刷新本页面
                  }
            }
        });
    }
</script>
EOF;
        $hidden = '<input type="hidden" name="form_id" value="'.$form['id'].'"/>'.$tokenStr;
        $result['form_id'] = $form['id'];
        $result['form_name'] = $form_name;
        $result['hidden'] = $hidden;
        $result['action'] = $action;
        $result['submit'] = "return {$submit}();";
        return [$result];
    }
}