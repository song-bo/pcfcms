<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;
use think\Config;
use think\Cookie;
use think\facade\Db;
// 基类
class Base
{
    // 主体语言（语言列表中最早一条）
    public $main_lang = 'cn';

    // 前台当前语言
    public $home_lang = 'cn';

    // 子目录
    public $root_dir = '';

    //构造函数
    function __construct()
    {
        // 控制器初始化
        $this->_initialize();
    }

    // 初始化
    protected function _initialize()
    {
        // 子目录安装路径
        $this->root_dir = root_path();
    }

    // 在typeid传值为目录名称的情况下，获取栏目ID
    public function getTrueTypeid($typeid = '')
    {
        if(is_array($typeid)){
            if (!empty($typeid)){
                $typeid = Db::name('Arctype')->where('dirname','IN',$typeid)->value('id');
            }
            return $typeid; 
        }else{
            // tid为目录名称的情况下
            if (!empty($typeid) && strval($typeid) != strval(intval($typeid))){
                $typeid = Db::name('Arctype')->where('dirname',$typeid)->value('id');
            }
            return $typeid;            
        }
    }
}