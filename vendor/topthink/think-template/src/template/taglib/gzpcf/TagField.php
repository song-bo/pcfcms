<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;
use app\common\model\ChannelType;
use think\facade\Db;

class TagField extends Base
{
    public $aid = '';
    //初始化
    protected function _initialize()
    {
        parent::_initialize();
        $this->aid = input('param.aid/d', 0);// 应用于文档列表
    }

    // 获取字段值
    public function getField($addfields = '', $aid = '')
    {
        $aid = !empty($aid) ? $aid : $this->aid;
        if (empty($aid)) {
            echo '标签field报错：缺少属性 aid 值，或文档ID不存在。';
            return false;
        }
        if (empty($addfields)) {
            echo '标签field报错：缺少属性 addfields 值。';
            return false;
        }
        $addfields = str_replace('，', ',', $addfields);
        $addfields = trim($addfields, ',');
        $addfields = explode(',', $addfields);
        $parseStr = '';
        $archivesRow = Db::name('archives')->field('typeid,channel')->where('aid',$aid)->find();
        if (empty($archivesRow)) {
            return $parseStr;
        }
        $channel = $archivesRow['channel'];
        // 获取栏目绑定的自定义字段ID列表
        $field_ids = Db::name('channelfield_bind')->where('typeid' ,'IN', [0,$archivesRow['typeid'])->column('field_id');
        if (empty($field_ids)) {
            $fieldname = current($addfields);
        } else {
            $map= [];
            $map[]= ['id','=',$field_ids];
            $map[]= ['name','=',$addfields];
            $map[]= ['channel_id','=',$channel];
            // 获取栏目对应的频道下指定的自定义字段
            $row = Db::name('channelfield')->where($map)->column('name', 'name');
            foreach ($addfields as $key => $val) {
                if (!empty($row[$val])) {
                    $fieldname = $val;
                    break;
                }
            }
        }
        // 附加表
        if (!empty($fieldname)) {
            // 自定义字段的类型
            $map1= [];
            $map1[]= ['name','=',$fieldname];
            $map1[]= ['channel_id','=',$channel];
            $dtype = Db::name('channelfield')->where($map1)->value('dtype');
            $Channeltype = new ChannelType();
            $channelInfo = $Channeltype->getInfo($channel);
            $tableContent = $channelInfo['table'].'_content';
            $parseStr = Db::name($tableContent)->where('aid',$aid)->value($fieldname);
            if ('htmltext' == $dtype) {
                $parseStr = htmlspecialchars_decode($parseStr);
            }
        }
        return $parseStr;
    }
}