<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;
use app\common\model\Arctype;
use think\facade\Db;

class TagPosition extends Base
{
    public $tid = '';
    
    //初始化
    protected function _initialize()
    {
        parent::_initialize();
        $this->tid = input("param.tid/s", ''); // 应用于栏目列表
        // 应用于文档列表
        $aid = input('param.aid/d', 0);
        if ($aid > 0) {
            $this->tid = Db::name('archives')->where('aid', $aid)->value('typeid');
        }
        // tid为目录名称的情况下
        $this->tid = $this->getTrueTypeid($this->tid);
    }

    // 获取面包屑位置
    public function getPosition($typeid = '', $symbol = '', $style = 'crumb')
    {
        $pcfglobal = get_global();
        $typeid = !empty($typeid) ? $typeid : $this->tid;
        $basicConfig = tpCache('basic');
        $basic_indexname = !empty($basicConfig['basic_indexname']) ? $basicConfig['basic_indexname'] : '首页';
        $symbol = !empty($symbol) ? $symbol : $basicConfig['list_symbol'];
        $seoConfig = tpCache('seo');
        $seo_pseudo = !empty($seoConfig['seo_pseudo']) ? $seoConfig['seo_pseudo'] : $pcfglobal['admin_config']['seo_pseudo'];
        // 动态模式
        if (1 == $seo_pseudo) {
            $home_url = request()->domain(); // 支持子目录
        } 
        // 静态模式
        else if (2 == $seo_pseudo) { 
            $home_url = request()->domain();
        } else {
            $home_url = request()->domain();
        }
        $str = "<a href='{$home_url}' class='{$style}'>{$basic_indexname}</a>";
        $Arctype = new Arctype();
        $result = $Arctype->getAllPid($typeid);
        $i = 1;
        foreach ($result as $key => $val) {
            if ($i < count($result)) {
                $str .= " {$symbol} <a href='{$val['typeurl']}' class='{$style}'>{$val['typename']}</a>";
            } else {
                $str .= " {$symbol} <a href='{$val['typeurl']}'>{$val['typename']}</a>";
            }
            ++$i;
        }
        return $str;
    }
}