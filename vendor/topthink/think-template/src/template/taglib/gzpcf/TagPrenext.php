<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;
use think\facade\Db;
use think\facade\Request;
use app\common\model\ChannelType;

// 内容页上下篇
class TagPrenext extends Base
{
    public $aid = 0;
    
    //初始化
    protected function _initialize()
    {
        parent::_initialize();
        $this->ChannelType = new ChannelType();
        $this->aid = input("param.aid/d", 0);
    }

    /**
     * 获取内容页上下篇
     */
    public function getPrenext($get = 'pre')
    {
        $aid = $this->aid;
        if (empty($aid)) {
            echo '标签prenext报错：只能用在内容页。';
            return false;
        }
        $channelRes = $this->ChannelType->getInfoByAid($aid);
        $channel = $channelRes['channel'];
        $typeid = $channelRes['typeid'];
        $controller_name = $channelRes['ctl_name'];
        if ($get == 'next') {
            $map=[];
            $map[]=['a.typeid','=',$typeid];
            $map[]=['a.aid','>',$aid];
            $map[]=['a.channel','=',$channel];
            $map[]=['a.status','=',1];
            $map[]=['a.is_del','=',0];
            $map[]=['a.arcrank','>=',0];
            // 下一篇
            $result = Db::name('archives')->field('b.*, a.*')
                ->alias('a')
                ->join('arctype b', 'b.id = a.typeid', 'LEFT')
                ->where($map)
                ->order('a.aid asc')
                ->find();
            if (!empty($result)) {
                $result['arcurl'] = arcurl($controller_name.'/view', $result);
                /*封面图*/
                if (empty($result['litpic'])) {
                    $result['is_litpic'] = 0; // 无封面图
                } else {
                    $result['is_litpic'] = 1; // 有封面图
                }
                $result['litpic'] = get_default_pic($result['litpic']); // 默认封面图
            }
        } else {
            $map1=[];
            $map1[]=['a.typeid','=',$typeid];
            $map1[]=['a.aid','<',$aid];
            $map1[]=['a.channel','=',$channel];
            $map1[]=['a.status','=',1];
            $map1[]=['a.is_del','=',0];
            $map1[]=['a.arcrank','>=',0];
            // 上一篇
            $result = Db::name('archives')->field('b.*, a.*')
                ->alias('a')
                ->join('arctype b', 'b.id = a.typeid', 'LEFT')
                ->where($map1)
                ->order('a.aid desc')
                ->find();
            if (!empty($result)) {
                $result['arcurl'] = arcurl($controller_name.'/view', $result);
                /*封面图*/
                if (empty($result['litpic'])) {
                    $result['is_litpic'] = 0; // 无封面图
                } else {
                    $result['is_litpic'] = 1; // 有封面图
                }
                $result['litpic'] = get_default_pic($result['litpic']); // 默认封面图
            }
        }
        return $result;
    }
}